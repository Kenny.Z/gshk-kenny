const express = require("express");
const router = express.Router();

const testDao = require("../modules/test-dao.js");
const upload = require('../modules/multer-uploader');
router.get("/", async function(req, res) {

    res.locals.title = "GSHK Project!";
    res.locals.allTestData = await testDao.retrieveAllTestData();

    res.render("home");
});
router.get("/blog", function(req, res) {

    res.render("blog");
});

module.exports = router;